# Markdown template

This repository contains a template to create a manuscript for submission to a scientific journal, written with markdown. The output is a Word document, formatted like you expect (double spaced, numbered lines, etc.).

It uses the rmarkdown package. Best used within Rstudio.
